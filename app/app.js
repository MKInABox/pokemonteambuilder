import Vue from 'nativescript-vue';

import PokeList from './components/PokeList';

// Uncommment the following to see NativeScript-Vue output logs
// Vue.config.silent = false;

new Vue({

    template: `
        <Frame>
            <PokeList />
        </Frame>`,

    components: {
        PokeList
    }
}).$start();
