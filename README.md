Starting the app:
- Prerequisites: have nativescritp installed on your pc, NativeScript Playground and NativeScript Preview apps installed on an android phone
- run "tns preview" command in the cloned project's directory
- scan the QR code given on the command line with the NativeScript Playground application
- After scanning, the NativeScript Preview application will open and you can view the application

If you have any problems with running the app, check:
https://nativescript.org/blog/nativescript-preview/